# Introduction

This is a demo project which includes a stand-alone pipeline to mine and store data found in the 
Experimental Factor Ontology (EFO) https://www.ebi.ac.uk/efo/

The pipelines includes a Postgres server and a python pipeline


# Requirements

Requires a PC or VM with:

- docker 
- docker-compose 

(tested with: Docker version 19.03.6, build 369ce74a3c)

(tested with: docker-compose version 1.25.0, build unknown)


# Instructions

To build the project, navigate into the project root and do:

    docker-compose build

This will pull the required images and build the Python pipeline

To start the Postgres server and run the pipeline, do:

    docker-compose up

Script parameters can be edited in the `pipeline/settings.py`

Note: This build an all-included Docker image. Thus, altering any settings or Python code will
require the project to be *rebuilt*

Note: By default, the database container mounts the relative path `database/files` in order
to persist data. This directory might need to be created on host machine. Otherwise, the
database persistence directory on host can be set in `docker-compose.yml` under db volumes.
