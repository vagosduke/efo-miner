
import os

SCRIPT_WAIT_TIME = 2        # seconds to wait before starting the script. This should ensure db is up

DATABASE_HOST = os.getenv('DATABASE_HOST', 'localhost')
DATABASE_NAME = os.getenv('DATABASE_NAME', 'localhost')
DATABASE_USERNAME = os.getenv('DATABASE_USERNAME', 'postgres')
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD', 'password')

EFO_ONTOLOGY_GET_TERMS_LIST_TEMPLATE = "http://www.ebi.ac.uk/ols/api/ontologies/efo/terms"

EFO_GET_PAGE_SIZE = 20
EFO_GET_PAGE_RETRY_TIMES = 3

WORKER_NUM = 4      # set to 0 to disable threading

EFO_ONTOLOGY_GET_TERM_TEMPLATE = "http://www.ebi.ac.uk/efo/EFO_1000862/parents"
