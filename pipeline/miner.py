import sys
import threading
import queue
import logging

from api_utils import get_efo_page, get_raw

import settings

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import Base, EfoTerm, EfoSynonym, EfoParentTemp, efo_parent


class EfoMinerThread(threading.Thread):
    def __init__(self, worker_id, work_queue, db_engine):
        threading.Thread.__init__(self)
        self.worker_id = worker_id
        self.work_queue = work_queue
        self.db_engine = db_engine
        self.session_maker = None       # will initialize after starting

    def run(self):
        self.session_maker = sessionmaker(bind=self.db_engine)
        while True:
            next_page = self.work_queue.get()
            if next_page is not '':
                logging.debug("Worker '{!s}' retrieving EFO page '{!s}'...".format(self.worker_id, next_page))
                efo_page = get_efo_page(page=next_page, size=settings.EFO_GET_PAGE_SIZE)
                if efo_page:
                    logging.debug("Worker '{!s}' Completed retrieval of EFO page '{!s}'".format(
                        self.worker_id, next_page))
                    logging.debug("Worker '{!s}' storing EfoTerm entries".format(self.worker_id))
                    self._extract_and_store_terms(efo_page, next_page)
                    logging.debug("Worker '{!s}' storing EfoSynonym entries".format(self.worker_id))
                    self._extract_and_store_synonyms(efo_page, next_page)
                    logging.debug("Worker '{!s}' storing EfoParentTemp entries".format(self.worker_id))
                    self._extract_and_store_temp_parents(efo_page, next_page)
                    logging.debug("...Worker '{!s}' Completed storage of EFO page '{!s}'".format(
                        self.worker_id, next_page))
                else:
                    logging.error("Worker '{!s}' Could not retrieve EFO page '{!s}'".format(
                        self.worker_id, next_page))
            else:
                break

    def _extract_and_store_terms(self, page_json, page_num):
        """ Extract data from EFO page JSON (as returned from the API) """
        try:
            page_term_list = page_json.get('_embedded', {}).get('terms', [])
            term_entry_list = []
            for pt in page_term_list:
                description = pt.get('description')[0] if pt.get('description') else ""
                if pt.get('obo_id') is None or pt.get('label') is None:
                    logging.error("Problem with obo_id:{!s}   label:{!s}. on page {:d}... Skipping".format(
                        pt.get('obo_id'), pt.get('label'), page_num))
                    continue
                entry = EfoTerm(
                    obo_id=pt.get('obo_id'),
                    term_label=pt.get('label'),
                    iri=pt.get('iri'),
                    description=description,
                    is_obsolete=pt.get('is_obsolete'),
                    replaced_by=pt.get('replaced_by')
                )
                term_entry_list.append(entry)
        except Exception as e:
            logging.error("Error during EfoTerm parsing of page '{:d}': {!s}".format(page_num, e))
            return
        try:
            session = self.session_maker()
            logging.debug("Adding '{:d}' EfoTerm entries in database".format(len(term_entry_list)))
            session.add_all(term_entry_list)
            session.commit()
        except Exception as e:
            logging.error("Error during EfoTerm storing of page '{:d}': {!s}".format(page_num, e))
            return

    def _extract_and_store_synonyms(self, page_json, page_num):
        try:
            page_term_list = page_json.get('_embedded', {}).get('terms', [])
            synonym_entry_list = []
            for pt in page_term_list:
                if pt.get('obo_id') is None:
                    # won't log. Detected in other steps
                    continue
                if pt.get('synonyms'):
                    for synonym in pt.get('synonyms', []):
                        if synonym:
                            entry = EfoSynonym(
                                obo_id=pt.get('obo_id'),
                                term_label=synonym,
                            )
                            synonym_entry_list.append(entry)
        except Exception as e:
            logging.error("Error during EfoSynonym parsing of page '{:d}': {!s}".format(page_num, e))
            return
        try:
            session = self.session_maker()
            logging.debug("Adding '{:d}' EfoSynonym entries in database".format(len(synonym_entry_list)))
            session.add_all(synonym_entry_list)
            session.commit()
        except Exception as e:
            logging.error("Error during EfoSynonym storing of page '{:d}': {!s}".format(page_num, e))
            return

    def _extract_and_store_temp_parents(self, page_json, page_num):
        try:
            page_term_list = page_json.get('_embedded', {}).get('terms', [])
            temp_parent_list = []
            for pt in page_term_list:
                if pt.get('obo_id') is None:
                    # won't log. Detected in other steps
                    continue
                parents_link = pt.get('_links', {}).get('parents', {}).get('href')
                if parents_link:
                    parents_json = get_raw(parents_link)
                    parents_list = parents_json.get('_embedded', {}).get('terms', [])
                    for parent_term in parents_list:
                        if parent_term.get('obo_id'):
                            entry = EfoParentTemp(
                                obo_id=pt.get('obo_id'),
                                parent_obo_id=parent_term.get('obo_id'),
                            )
                            temp_parent_list.append(entry)
        except Exception as e:
            logging.error("Error during EfoParentTemp parsing of page '{:d}': {!s}".format(page_num, e))
            return
        try:
            session = self.session_maker()
            logging.debug("Adding '{:d}' EfoParentTemp entries in database".format(len(temp_parent_list)))
            session.add_all(temp_parent_list)
            session.commit()
        except Exception as e:
            logging.error("Error during EfoParentTemp storing of page '{:d}': {!s}".format(page_num, e))
            return


class EfoMiner:
    db_engine = None
    db_conn = None
    work_queue = queue.Queue()

    def __init__(self):
        self.initialize_db_connection()

    def initialize_db_connection(self):
        """
        Use environment variables to initialize a Postgres connection
        """
        try:
            logging.debug('Connecting to the PostgreSQL database...')
            db_connection_string = "postgresql://{user}:{password}@{host}/{db_name}".format(
                user=settings.DATABASE_USERNAME,
                password=settings.DATABASE_PASSWORD,
                host=settings.DATABASE_HOST,
                db_name=settings.DATABASE_NAME
            )
            self.db_engine = create_engine(db_connection_string, echo=True)
            self.db_conn = self.db_engine.connect()
            db_version = self.db_conn.execute('SELECT version()').fetchall()
            logging.debug('PostgreSQL database version:')
            logging.debug(db_version)   # display the PostgreSQL database server version
        except Exception as e:
            logging.critical("FATAL: Could not initialize database connection: {!s}".format(e))
            sys.exit(1)
        return self.db_conn

    def migrate_database(self):
        try:
            logging.info("Migrating EFO database schema")
            Base.metadata.create_all(self.db_engine)
        except Exception as e:
            logging.critical('FATAL: Could not migrate db: {!s}'.format(e))
            sys.exit(1)
        else:
            logging.info("EFO database migration completed without errors (or was not needed)")

    def start_ontology_mining(self):
        logging.info("--- Starting Ontology mining from EFO API")
        self._mine_terms_and_synonyms()
        logging.info("--- Ontology mining from EFO API completed! Will now re-map the parents")
        self._map_parents()
        logging.info("--- EFO term parent mapping completed!")

    def close_connection(self):
        try:
            self.db_conn.close()
        except Exception as e:
            logging.warning('Could not close db connection: {!s}'.format(e))

    # Private functions
    def _mine_terms_and_synonyms(self):
        """
        Do an initial fetch to get the number of pages and store the work to be done.
        Then, spawn synchronous or threaded workers (depending on the settings)
        """
        efo_page = get_efo_page(size=settings.EFO_GET_PAGE_SIZE)   # do initial fetch with page-size to get workload
        if not efo_page:
            logging.error("Cannot fetch initial EFO page. Cannot continue")
            return
        # initialize work-to-be-done and workers
        total_page_num = efo_page.get('page', {}).get('totalPages')
        logging.info("Starting synchronization")
        logging.info("Will do '{:d}' pages of size '{:d}' with '{:d}' workers".format(
            total_page_num, settings.EFO_GET_PAGE_SIZE, settings.WORKER_NUM))
        for p in range(total_page_num):
            self.work_queue.put(p)
        if settings.WORKER_NUM > 0:
            worker_list = []
            for i in range(1, settings.WORKER_NUM+1):
                logging.info("Creating synchronization worker {:d}".format(i))
                sync_thread = EfoMinerThread(i, self.work_queue, self.db_engine)
                worker_list.append(sync_thread)
                sync_thread.start()
            logging.info("Waiting workers to finish...")
            for worker in worker_list:
                worker.join()
                logging.debug("Worker '{!s}' joined".format(worker.worker_id))
            logging.info("...All workers joined")
        else:
            logging.info("Worker num = 0. Creating same-thread worker")
            sync_thread = EfoMinerThread(0, self.work_queue, self.db_engine)
            sync_thread.run()

    def _map_parents(self):
        """
        Do a final step to migrate term parent relations from efo_parents_temp to efo_parents
        and thus properly structuring relationship with foreign keys
        """
        session_maker = sessionmaker(bind=self.db_engine)
        session = session_maker()
        for temp_parent in session.query(EfoParentTemp).all():
            efo_term_parent = session.query(EfoTerm).filter(EfoTerm.obo_id==temp_parent.parent_obo_id).first()
            efo_term = session.query(EfoTerm).filter(EfoTerm.obo_id==temp_parent.obo_id).first()
            if efo_term and efo_term_parent:
                efo_term.parents.append(efo_term_parent)
            else:
                logging.warning("Missing term ({!s}) - parent ({!s}) relation".format(efo_term, efo_term_parent))
        session.commit()

