"""
This script will initialize a database to store EFO terms, synonyms and hierarchy.

1. Migrate the database using SQLAlchemy if the database does not exist already
2. Spawn threaded workers to access EFO API
3. Each worker fetches an API page, parses the page and stores the data to the corresponding DB tables

process settings are found in settings.py
"""

import time
import logging

import settings
from miner import EfoMiner

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("urllib3.connectionpool").setLevel(logging.WARNING)
    logging.info("Hi")

    time.sleep(settings.SCRIPT_WAIT_TIME)           # timer to ensure that the database container is up
    synchronizer = EfoMiner()
    synchronizer.migrate_database()
    synchronizer.start_ontology_mining()
    synchronizer.close_connection()
