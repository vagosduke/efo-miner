import requests
import logging

import settings


def get_efo_page(page=0, size=None):
    url = settings.EFO_ONTOLOGY_GET_TERMS_LIST_TEMPLATE
    params = {
        'page': page,
        'size': size if size else settings.EFO_GET_PAGE_SIZE
    }
    headers = {
        'Accept': 'application/json'
    }
    retry_num = settings.EFO_GET_PAGE_RETRY_TIMES
    while retry_num > 0:
        try:
            req = requests.get(url, params=params, headers=headers)
            return req.json()
        except Exception as e:
            logging.warning("Could not perform GET request to '{!s} {!s}'\n{!s}".format(url, params, e))
            logging.warning("Retrying... {:d}/{:d}".format(settings.EFO_GET_PAGE_RETRY_TIMES - retry_num + 1,
                                                 settings.EFO_GET_PAGE_RETRY_TIMES))
            retry_num -= 1
    logging.error("GET request to '{!s} {!s}' failed after {:d} retries".format(
        url, params, settings.EFO_GET_PAGE_RETRY_TIMES))
    return None


def get_raw(url):
    headers = {
        'Accept': 'application/json'
    }
    retry_num = settings.EFO_GET_PAGE_RETRY_TIMES
    while retry_num > 0:
        try:
            req = requests.get(url, headers=headers)
            return req.json()
        except Exception as e:
            logging.warning("Could not perform GET request to '{!s}'\n{!s}".format(url, e))
            logging.warning("Retrying... {:d}/{:d}".format(settings.EFO_GET_PAGE_RETRY_TIMES - retry_num + 1,
                                                           settings.EFO_GET_PAGE_RETRY_TIMES))
            retry_num -= 1
    logging.error("GET request to '{!s}' failed after {:d} retries".format(
        url, settings.EFO_GET_PAGE_RETRY_TIMES))
    return None
