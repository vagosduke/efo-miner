

from sqlalchemy import Column, Integer, String, Text, Boolean, PrimaryKeyConstraint, ForeignKey, INTEGER, Table
from sqlalchemy.orm import declarative_base, relationship, relation

Base = declarative_base()


class EfoSynonym(Base):
    __tablename__ = 'efo_synonyms'

    obo_id = Column(String, ForeignKey('efo_terms.obo_id'), primary_key=True)
    term_label = Column(String, primary_key=True)


# class EfoParent(Base):
#     __tablename__ = 'efo_parents'
#     __table_args__ = (
#         PrimaryKeyConstraint('obo_id', 'parent_obo_id'),
#         {},
#     )
#     # id = Column(Integer, primary_key=True, autoincrement=True)
#     obo_id = Column(String, ForeignKey('efo_terms.obo_id'), primary_key=True)
#     parent_obo_id = Column(String, ForeignKey('efo_terms.obo_id'), primary_key=True)

efo_parent = Table(
    'efo_parents',
    Base.metadata,
    Column('obo_id', String, ForeignKey('efo_terms.obo_id'), primary_key=True),
    Column('parent_obo_id', String, ForeignKey('efo_terms.obo_id'), primary_key=True)
)


class EfoTerm(Base):
    __tablename__ = 'efo_terms'

    obo_id = Column(String, primary_key=True)
    term_label = Column(String, unique=True)
    iri = Column(String)
    description = Column(Text)
    is_obsolete = Column(Boolean, default=False)
    replaced_by = Column(String)
    synonyms = relationship("EfoSynonym")
    parents = relationship("EfoTerm", secondary=efo_parent,
                           primaryjoin=efo_parent.c.obo_id==obo_id,
                           secondaryjoin=efo_parent.c.parent_obo_id==obo_id)

    def __repr__(self):
        return "<EfoTerm(obo_id='%s', label='%s', iri='%s')>" % (self.obo_id, self.term_label, self.iri)

    def __str__(self):
        return self.__repr__()


class EfoParentTemp(Base):
    """
    Temporary table to store child-parent relations without foreign key constrains
    """

    __tablename__ = 'efo_parents_temp'

    obo_id = Column(String, primary_key=True)
    parent_obo_id = Column(String, primary_key=True)

